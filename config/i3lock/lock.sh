#!/bin/bash
if [[ "$DESKTOP_SESSION" == "qtile" ]]; then
    wall="$HOME/.config/i3lock/lock.png"
elif [[ "$DESKTOP_SESSION" == "xmonad" ]]; then
    wall="$HOME/.config/i3lock/lock.png"
elif [[ "$DESKTOP_SESSION" == "dwm" ]]; then
    wall="$HOME/.config/i3lock/lock.png"
elif [[ "$DESKTOP_SESSION" == "bspwm" ]]; then
    wall="$HOME/.config/i3lock/lock.png"
fi
    i3lock \
    -t -i $wall \
	-b -e \
	--no-modkey-text \
	--pass-media-keys \
	--pass-power-keys \
	\
	--radius 120 \
	\
	--clock \
	--force-clock \
	--time-pos x+1850:y+h-960 \
	--time-color ffffffff \
	--time-size 72 \
	--date-size 22 \
	--date-pos tx+20:ty+920 \
	--date-color ffffffff \
	--date-align 2 \
	--time-align 2 \
	--time-font AvantGarde \
	--date-font AvantGarde \
	\
	--status-pos x+5:y+h-16 \
	--separator-color 00000000 \
	\
	--ring-color 008000ff \
	--inside-color 00000099 \
	--ringver-color 008000ff \
	--insidever-color 000000CC \
	--insidewrong-color 000000CC \
	--ringwrong-color ff0000ff \
	\
	--line-color ffffff00 \
	\
	--verif-text Verifying... \
	--wrong-text Wrong! \
	--verif-color ffffffff \
	--wrong-color ffffffff \
	\
	--noinput-text "No input" \
	--lock-text Locking… \
	--lockfailed-text "Lock failed!" \
	--greeter-text "Type Password to Unlock" \
	--greeter-font AvantGarde \
	--greeter-color ffffffff \
	--greeter-size 24 \
	--greeter-pos tx+1030:ty+630 \
