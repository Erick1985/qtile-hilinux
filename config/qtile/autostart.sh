#!/usr/bin/env bash 
lxsession &
killall picom &>/dev/null
picom --experimental-backends --config $HOME/.config/qtile/picom.conf &
xsetroot -cursor_name left_ptr &
xset r rate 200 30 &
killall applet.py &
BG=$HOME/.config/fehbg-qtile
if [ -f "$BG" ]; then
    ~/.config/fehbg-qtile &
else
    feh --no-fehbg --bg-tile $HOME/.config/qtile/wallpaper.jpg &
    notify-send "Wallpaper" "Default wallpaper selected.\nPress Super+F1 to choose one.\nWallpaper Directory: ~/.wallpapers" &
fi
killall pnmixer &>/dev/null
pnmixer &
nm-applet &
xbindkeys &
killall imwheel &>/dev/null
imwheel -b 45 &
~/.scripts/gsimplecal/gsimplecal.sh &
megasync &
killall mpd &>/dev/null
mpd &
sleep 30 && ~/.scripts/checkmail/checkmail-cron.sh &
sleep 30 && ~/.scripts/checkcal/checkcal-cron.sh &
# sleep 20 && /usr/bin/emacs --daemon &
