#!/bin/bash
##  _   _  _  _      _                      _ 
## | | | |(_)| |    (_) _ __   _   _ __  __| | 
## | |_| || || |    | || '_ \ | | | |\ \/ /| | 
## |  _  || || |___ | || | | || |_| | >  < |_| 
## |_| |_||_||_____||_||_| |_| \__,_|/_/\_\(_) 
## 
##                     https://hilinux.com.br

time="$(date +%a), $(date +%b) $(date +%d) | $(date +%H:%M:%S)"

### Run Calcurse-Caldav #########################
killall calcurse-caldav &>/dev/null   # do not duplicate
sleep 1
run=$(calcurse-caldav 2>&1)
### Test if Sync succeed
if [ $? -eq 1 ]; then
  notify-send "Calendar Synchronization Failed" "\n\nError Message(s):\n\n$run\n\n$time" \
      --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 1
fi
