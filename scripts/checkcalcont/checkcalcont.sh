#!/bin/bash
##  _   _  _  _      _                      _ 
## | | | |(_)| |    (_) _ __   _   _ __  __| | 
## | |_| || || |    | || '_ \ | | | |\ \/ /| | 
## |  _  || || |___ | || | | || |_| | >  < |_| 
## |_| |_||_||_____||_||_| |_| \__,_|/_/\_\(_) 
## 
##                     https://hilinux.com.br

source $HOME/.scripts/_dmenu/dmenu-colors;

scriptpath="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd )"
scriptname="$( ls $scriptpath | grep checkcalcont )"

time="$(date +%a), $(date +%b) $(date +%d) | $(date +%H:%M:%S)"

sync_calendar () {
### Run Calcurse-Caldav #########################
killall calcurse-caldav &>/dev/null   # do not duplicate
sleep 1
run_cal=$(calcurse-caldav 2>&1)
### Test if Sync succeed
if [ $? -eq 1 ]; then
  notify-send "Calendar Synchronization Failed" "\nError Message(s):\n\n$run_cal\n\n$time" \
      --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg"
else
  notify-send "Calendar Synced Successfuly" "Reload Calcurse to view changes.\n\n$time" \
      --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/process-completed-symbolic.svg"
fi
}

sync_contacts () {
run_cont_conv=$(~/.abook/convert-cont.sh)
if [ $? -eq 1 ]; then
    notify-send "Contacts Convertion Failed" "\nContacts synced successfuly, but\nconversion has failed.\n\nError Message(s):\n\n$run_cont_conv\n\n$time" \
        --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg"
else
    notify-send "Contacts Synced Successfuly" "Reload Abook to view changes.\n\n$time" \
        --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/process-completed-symbolic.svg"
fi
}

declare -a sync=(
"1 - Calendar"
"2 - Contacts"
"3 - Both"
)

choice=$(printf '%s\n' "${sync[@]}" | ${DMENU} -l 3 -p 'Sync ') "$@"
if [[ $choice ]]; then
  if [[ $choice = "1 - Calendar" ]]; then
    sync_calendar && exit 0
  elif [[ $choice = "2 - Contacts" ]]; then
    sync_contacts && exit 0
  elif [[ $choice = "3 - Both" ]]; then
    sync_calendar
    sync_contacts
    exit 0
  else
      $scriptpath/$scriptname && exit 0
  fi
else
    echo "User chose no to Sync. Program Terminated." && exit 0
fi
