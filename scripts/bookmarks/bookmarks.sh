#!/bin/bash
##  _   _  _  _      _                      _ 
## | | | |(_)| |    (_) _ __   _   _ __  __| | 
## | |_| || || |    | || '_ \ | | | |\ \/ /| | 
## |  _  || || |___ | || | | || |_| | >  < |_| 
## |_| |_||_||_____||_||_| |_| \__,_|/_/\_\(_) 
## 
##                     https://hilinux.com.br
##

source $HOME/.scripts/_dmenu/dmenu-colors;

# Load bookmarks from Qutebrowser
url_list=$(awk '{for (i=2; i<NF; i++) printf $i" "; if (NF >= 2) print $NF"   -   "$1}' "/home/smsjr/.config/qutebrowser/bookmarks/urls" | sort -b -d -f)
# Clean empty lines from url list (sed...) and prompt Dmenu for user choice
choice=$( printf '%s\n' "${url_list}" | sed '/^[[:space:]]*$/d' | ${DMENU} -i -l 10 -p 'Bookmarks ' "$@" )
# Extract url from user choice
if [ -n "$choice" ]; then
  [ -z "$choice" ] && exit 1
  url=$(echo "${choice}" | awk '{print $NF}') || exit 1
else
  echo "User chose to quit. Program terminated." && exit 0
fi
# Choose browser to open chosed url
declare -a browsers=(
    "Qutebrowser"
    "Brave Browser"
    "Mozilla Firefox"
)
browser=$(printf '%s\n' "${browsers[@]}" | ${DMENU} -l 4 -p 'Which Browser? ')
if [ -n "$browser" ]; then
  if [[ "$browser" == "Qutebrowser" ]]; then
    BROWSER="qutebrowser"
  elif [[ "$browser" == "Brave Browser" ]]; then
    BROWSER="brave"
  elif [[ "$browser" == "Mozilla Firefox" ]]; then
    BROWSER="firefox"
  else
      notify-send "Browser" "Invalid browser. Please try again." \
          --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 1
  fi
else
  echo "User chose to quit. Program Terminated." && exit 0
fi
# Check empty url and open the url in the chosed browser
[ -z "$url" ] && \
    notify-send "URL" "Blank URL. Please try again." \
        --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 1 || \
            nohup "$BROWSER" "${url}" >/dev/null 2>&1 &
