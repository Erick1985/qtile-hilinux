#!/bin/bash
##  _   _  _  _      _                      _ 
## | | | |(_)| |    (_) _ __   _   _ __  __| | 
## | |_| || || |    | || '_ \ | | | |\ \/ /| | 
## |  _  || || |___ | || | | || |_| | >  < |_| 
## |_| |_||_||_____||_||_| |_| \__,_|/_/\_\(_) 
## 
##                     https://hilinux.com.br

source $HOME/.scripts/_dmenu/dmenu-colors;

if [[ "$(echo -e "No\nYes" | ${DMENU} -p "Clean Qutebrowser? ")" == "Yes" ]]; then
    if [[ -d ~/.cache/qutebrowser ]] && [[ -d ~/.local/share/qutebrowser ]]; then
    	rm -rf ~/.cache/qutebrowser 
	    rm -rf ~/.local/share/qutebrowser 
    	notify-send "Qutebrowser" "All cache and/or data cleaned!" --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/apps/web-browser-symbolic.svg"
    else
        notify-send "Qutebrowser" "No data or cache to clear." --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/apps/web-browser-symbolic.svg" && exit 0
    fi  
else
    echo "Program Terminated." && exit 0
fi  
