#!/bin/bash
##  _   _  _  _      _                      _ 
## | | | |(_)| |    (_) _ __   _   _ __  __| | 
## | |_| || || |    | || '_ \ | | | |\ \/ /| | 
## |  _  || || |___ | || | | || |_| | >  < |_| 
## |_| |_||_||_____||_||_| |_| \__,_|/_/\_\(_) 
## 
##                     https://hilinux.com.br

source $HOME/.scripts/_dmenu/dmenu-colors;

scriptpath="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd )"
scriptname="$( ls $scriptpath | grep sh )"

# Terminal
term="st"
termexec="-e"

manpages=$(man -k . -s 1 | awk '{$3="  -  "; print $0}' | sed 's/(1)//g' | sort -b -d -f )
choice="$( printf '%s\n' "${manpages[@]}" | ${DMENU} -l 10 -p "Man Page " )"
if [[ $choice ]]; then
    if [[ "${manpages[@]}" =~ "$choice" ]]; then
      manual="$(echo "${choice}" | awk '{print $2, $1}' | tr -d '()')"
      nohup man -T${FORMAT:-pdf} $manual | ${READER:-zathura -} >/dev/null 2>&1 &
    else
        $scriptpath/$scriptname && exit 0
    fi
else
    echo "Use chose to quit" && exit 0
fi
