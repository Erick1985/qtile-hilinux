#!/bin/bash
##  _   _  _  _      _                      _ 
## | | | |(_)| |    (_) _ __   _   _ __  __| | 
## | |_| || || |    | || '_ \ | | | |\ \/ /| | 
## |  _  || || |___ | || | | || |_| | >  < |_| 
## |_| |_||_||_____||_||_| |_| \__,_|/_/\_\(_) 
## 
##                     https://hilinux.com.br

source $HOME/.scripts/_dmenu/dmenu-colors;

scriptpath="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd )"
scriptname="$( ls $scriptpath )"

declare -a messengers=(
    "1 - Telegram"
    "2 - WhatsApp"
    "3 - WhatsApp Business"
    "4 - Telegram & WhatsApps"
    "5 - WhatsApps Only"
    "6 - Telegram & WhatsApp"
    "7 - Telegram & WhatsApp Business"
    "8 - All Messengers"
)

choice=$(printf '%s\n' "${messengers[@]}" | ${DMENU} -l 10 -p 'Messengers ');
if [ -n "$choice" ]; then
  if [[ "${messengers[@]}" =~ "$choice" ]]; then
    if [[ "$choice" == "1 - Telegram" ]]; then
      nohup ~/.Telegram/Telegram &
    elif [[ "$choice" == "2 - WhatsApp" ]]; then
      nohup ~/.WebApps/WhatsApp-linux-x64/WhatsApp &
    elif [[ "$choice" == "3 - WhatsApp Business" ]]; then
      nohup ~/.WebApps/WhatsAppBusiness-linux-x64/WhatsAppBusiness &
    elif [[ "$choice" == "4 - Telegram & WhatsApps" ]]; then
      nohup ~/.Telegram/Telegram & nohup ~/.WebApps/WhatsApp-linux-x64/WhatsApp & nohup ~/.WebApps/WhatsAppBusiness-linux-x64/WhatsAppBusiness &
    elif [[ "$choice" == "5 - WhatsApps Only" ]]; then
      nohup ~/.WebApps/WhatsApp-linux-x64/WhatsApp & nohup ~/.WebApps/WhatsAppBusiness-linux-x64/WhatsAppBusiness &
    elif [[ "$choice" == "6 - Telegram & WhatsApp" ]]; then
      nohup ~/.Telegram/Telegram & nohup ~/.WebApps/WhatsApp-linux-x64/WhatsApp &
    elif [[ "$choice" == "7 - Telegram & WhatsApp Business" ]]; then
      nohup ~/.Telegram/Telegram & nohup ~/.WebApps/WhatsAppBusiness-linux-x64/WhatsAppBusiness &
    elif [[ "$choice" == "8 - All Messengers" ]]; then
      nohup ~/.Telegram/Telegram & nohup ~/.WebApps/WhatsApp-linux-x64/WhatsApp & nohup ~/.WebApps/WhatsAppBusiness-linux-x64/WhatsAppBusiness &
    fi
  else
    $scriptpath/$scriptname && exit 0
  fi
else
  echo "User chose to quit. Program Terminated." && exit 0
fi
