#!/bin/bash
##  _   _  _  _      _                      _ 
## | | | |(_)| |    (_) _ __   _   _ __  __| | 
## | |_| || || |    | || '_ \ | | | |\ \/ /| | 
## |  _  || || |___ | || | | || |_| | >  < |_| 
## |_| |_||_||_____||_||_| |_| \__,_|/_/\_\(_) 
## 
##                     https://hilinux.com.br

source $HOME/.scripts/_dmenu/dmenu-colors;

scriptpath="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd )"
scriptname="$( ls $scriptpath )"

ytsearch="https://www.youtube.com/results?search_query="
BROWSER="chromium"

declare -a options=(
  "1 - Open YouTube"
  "2 - Search YouTube"
)

choice=$(printf '%s\n' "${options[@]}" | ${DMENU} -l 2 -p 'YouTube ') "$@"
if [[ $choice ]]; then
    if [[ "$choice" == "1 - Open YouTube" ]]; then
            nohup "$BROWSER" "https://youtube.com" > /dev/null 2>&1 &
    elif [[ "$choice" == "2 - Search YouTube" ]]; then
      query=$(echo "$ytsearch" | ${DMENU} -l 2 -p 'Enter search query:')
      if [ "$query" ]; then
          if [ "$query" = "$ytsearch" ]; then
            $scriptpath/$scriptname && exit 0
          else
            nohup "$BROWSER" "${ytsearch}${query}" > /dev/null 2>&1 &
          fi   
      else
          echo "User chose not to search. Program Terminated." && exit 0
      fi
    else
      $scriptpath/$scriptname && exit 0
    fi
else
  echo "Use chose to quit" && exit 0
fi
