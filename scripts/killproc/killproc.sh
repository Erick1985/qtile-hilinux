#!/bin/bash
##  _   _  _  _      _                      _ 
## | | | |(_)| |    (_) _ __   _   _ __  __| | 
## | |_| || || |    | || '_ \ | | | |\ \/ /| | 
## |  _  || || |___ | || | | || |_| | >  < |_| 
## |_| |_||_||_____||_||_| |_| \__,_|/_/\_\(_) 
## 
##                     https://hilinux.com.br

source $HOME/.scripts/_dmenu/dmenu-colors;

scriptpath="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd )"
scriptname="$( ls $scriptpath )"

main() {
processes="$(ps --user "$(id -u)" w | awk '{print $1"   "$5}')"
select="$( echo "$processes" | awk '{print $1"   "$2}' | ${DMENU} -l 10 -p "Process to kill ")";
if [[ -n $select ]]; then
    if [[ $select == "PID   COMMAND" ]]; then
        $scriptpath/$scriptname && exit 0
    else
        if [[ "$processes" =~ "$select" ]]; then
            selchoi="$(awk '{print $2}' <<< "$select")"
            answer="$(echo -e "No\nYes" | ${DMENU} -p "Kill  $selchoi ? " "$@")";
            if [[ $answer == "Yes" ]]; then
              selpid="$(awk '{print $1}' <<< "$select")";
              kill -9 "$selpid"
              echo "Process $selchoi has been killed."
              notify-send "Kill Process" "Process < $selchoi > has been killed!" --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/process-completed-symbolic.svg" && exit 0
            elif [[ $answer == "No" ]]; then
              echo "Program terminated." && exit 0
            else
              echo "Program terminated." && exit 0
            fi
        else
            $scriptpath/$scriptname && exit 0
            # notify-send "ERROR" "Invalid process. Please try again." --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 0
        fi
    fi
else
    echo "Program terminated." && exit 0
fi
}
[[ "${BASH_SOURCE[0]}" == "${0}" ]] && main "$@"
