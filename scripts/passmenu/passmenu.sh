#!/bin/bash
##  _   _  _  _      _                      _ 
## | | | |(_)| |    (_) _ __   _   _ __  __| | 
## | |_| || || |    | || '_ \ | | | |\ \/ /| | 
## |  _  || || |___ | || | | || |_| | >  < |_| 
## |_| |_||_||_____||_||_| |_| \__,_|/_/\_\(_) 
## 
##                     https://hilinux.com.br

source $HOME/.scripts/_dmenu/dmenu-colors;

scriptpath="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd )"
scriptname="$( ls $scriptpath )"

shopt -s nullglob globstar

typeit=0
if [[ $1 == "--type" ]]; then
	typeit=1
	shift
fi
if [[ -d ~/.password-store ]]; then
    prefix=${PASSWORD_STORE_DIR-~/.password-store}
    password_files=( "$prefix"/**/*.gpg )
    password_files=( "${password_files[@]#"$prefix"/}" )
    password_files=( "${password_files[@]%.gpg}" )
    password=$(printf '%s\n' "${password_files[@]}" | ${DMENU} -p "Passmenu " "$@")
    if [[ -n $password ]]; then
        if [[ ${password_files[@]} =~ $password ]]; then
            pass_cmd=show
            if pass show "$password" | grep -q '^otpauth://'; then
                pass_cmd=otp
            fi
            if [[ $typeit -eq 0 ]]; then
    	        pass $pass_cmd -c "$password" 2>/dev/null
            else
	            pass $pass_cmd "$password" | { IFS= read -r pass; printf %s "$pass"; } |
		            xdotool type --clearmodifiers --file -
            fi
        else
            $scriptpath/$scriptname && exit 0
        fi
    else
        echo "User chose to quit. Program Terminated." && exit 0
    fi
else
    notify-send "Password Store Does Not Exist" "Please verify and try again." --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 1
fi
