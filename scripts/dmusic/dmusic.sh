#!/bin/bash
##  _   _  _  _      _                      _ 
## | | | |(_)| |    (_) _ __   _   _ __  __| | 
## | |_| || || |    | || '_ \ | | | |\ \/ /| | 
## |  _  || || |___ | || | | || |_| | >  < |_| 
## |_| |_||_||_____||_||_| |_| \__,_|/_/\_\(_) 
## 
##                     https://hilinux.com.br
##

source $HOME/.scripts/_dmenu/dmenu-colors;

if ! pgrep -x mpd >/dev/null; then
  notify-send "Music" "MPD Daemon not running.\nPlease verify and try again." \
    --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 1
fi  

declare -a mode=(
    "1 - Full Album"
    "2 - Single Song"
)

artist_list="$(mpc list Artist)"
artist="$(echo "$artist_list" | ${DMENU} -l 15 -p 'Artist ')"
if [[ $artist ]]; then 
    if [[ "${artist_list[@]}" =~ "$artist" ]]; then
        choice="$(printf '%s\n' "${mode[@]}" | ${DMENU} -l 15 -p 'Mode ')" 
        if [ "$choice" = "1 - Full Album" ]; then
            album_list="$(mpc list Album Artist "$artist")"
            album="$(echo "$album_list" | ${DMENU} -l 15 -p 'Album ')" 
            if [[ $album ]] && [[ ${album_list[@]} =~ "$album" ]]; then
                mpc clear && mpc findadd Artist "$artist" Album "$album" && mpc play || exit 1
            else
                echo "Invalid or not selected album." && exit 0
            fi
        elif [ "$choice" = "2 - Single Song" ]; then
            song_list="$(mpc search Artist "$artist")"
            song="$(echo "$song_list" | ${DMENU} -l 15 -p 'Song ')" 
            if [[ $song ]] && [[ ${song_list[@]} =~ "$song" ]]; then
                mpc clear && mpc add "$song" && mpc play || exit 1
            else
                echo "Invalid or not selected song." && exit 0
            fi
        else
            echo "Invalid or not selected mode." && exit 0
        fi
    else
        echo "Invalid artist." && exit 0
    fi
else
    echo "Artist not selected."
    album_list="$(mpc list Album)"
    album="$(echo "$album_list" | ${DMENU} -l 15 -p 'Album ')" 
    if [[ $album ]] && [[ ${album_list[@]} =~ "$album" ]]; then
        mpc clear && mpc findadd Album "$album" && mpc play || exit 1
    else
        echo "Invalid or not selected album." && exit 0
    fi
fi
