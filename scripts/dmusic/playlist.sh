#!/bin/bash
##  _   _  _  _      _                      _ 
## | | | |(_)| |    (_) _ __   _   _ __  __| | 
## | |_| || || |    | || '_ \ | | | |\ \/ /| | 
## |  _  || || |___ | || | | || |_| | >  < |_| 
## |_| |_||_||_____||_||_| |_| \__,_|/_/\_\(_) 
## 
##                     https://hilinux.com.br
##

source $HOME/.scripts/_dmenu/dmenu-colors;

if ! pgrep -x mpd >/dev/null; then
  notify-send "Music" "MPD Daemon not running.\nPlease verify and try again." \
    --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 1
fi  

playlist_original="$(mpc playlist)"
playlist="$(echo "$playlist_original" | sed "s|\ \&\ |\ and\ |g")"
current_original="$(mpc current)"
current="$(echo "$current_original" | sed 's/\ \&\ /\ and\ /g')"
if [ -z "$current" ]; then
    playlist="$playlist"
else
    [ "$(mpc status %state%)" = "playing" ] && icon="  " || icon="  "
    playlist="$(echo "$playlist" | sed "s|$current|$icon $current|g")"
fi

choice="$(printf '%s\n' "${playlist[@]}" | ${DMENU} -l 15 -p 'Playlist ')" 
if [[ "$choice" ]] && [[ "${playlist[@]}" =~ "$choice" ]]; then
    if [ -z "$current" ]; then
        artist="$(echo "$choice" | sed 's/\ and\ /\ \&\ /g' | awk -F " - " '{print $1}')"
        song=  "$(echo "$choice" | sed 's/\ and\ /\ \&\ /g' | awk -F " - " '{print $2}')"
        mpc searchplay Artist "$artist" Title "$song"
    else
        if [ "${choice:4}" == "$current" ]; then
            artist="$(echo "${choice:4}" | sed 's/\ and\ /\ \&\ /g' | awk -F " - " '{print $1}')"
        else
            artist="$(echo "${choice}" |   sed 's/\ and\ /\ \&\ /g' | awk -F " - " '{print $1}')"
        fi
        song="$(echo "$choice" | sed 's/\ and\ /\ \&\ /g' | awk -F " - " '{print $2}')"
        mpc searchplay Artist "$artist" Title "$song"
    fi
else
    echo "Invalid choice or user chose to quit." && exit 0
fi
