#!/bin/bash
##  _   _  _  _      _                      _ 
## | | | |(_)| |    (_) _ __   _   _ __  __| | 
## | |_| || || |    | || '_ \ | | | |\ \/ /| | 
## |  _  || || |___ | || | | || |_| | >  < |_| 
## |_| |_||_||_____||_||_| |_| \__,_|/_/\_\(_) 
## 
##                     https://hilinux.com.br
##

source $HOME/.scripts/_dmenu/dmenu-colors;

if ! pgrep -x mpd >/dev/null; then
  notify-send "Music" "MPD Daemon not running.\nPlease verify and try again." \
    --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 1
fi  

declare -a mpc=(
    "   Toggle Play"
    "   Stop Playing"
    "   Next"
    "   Previous"
    "   Random"
    "   Repeat"
    "   Clear Queue"
)
choice="$(printf '%s\n' "${mpc[@]}" | ${DMENU} -l 7 -p 'Music ')" 
if [[ $choice = "   Toggle Play" ]]; then
    mpc toggle
elif [[ $choice = "   Stop Playing" ]]; then
    mpc stop
elif [[ $choice = "   Next" ]]; then
    mpc next
elif [[ $choice = "   Previous" ]]; then
    mpc prev
elif [[ $choice = "   Random" ]]; then
    mpc random
elif [[ $choice = "   Repeat" ]]; then
    mpc repeat
elif [[ $choice = "   Clear Queue" ]]; then
    mpc clear
else
    echo "User chose to quit." && exit 0
fi

