#!/bin/bash
##  _   _  _  _      _                      _ 
## | | | |(_)| |    (_) _ __   _   _ __  __| | 
## | |_| || || |    | || '_ \ | | | |\ \/ /| | 
## |  _  || || |___ | || | | || |_| | >  < |_| 
## |_| |_||_||_____||_||_| |_| \__,_|/_/\_\(_) 
## 
##                     https://hilinux.com.br

source $HOME/.scripts/_dmenu/dmenu-colors;

scriptpath="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd )"
scriptname="$( ls $scriptpath )"
filebrowser="thunar"

declare -a options=(
"1 - Clean Trash"
"2 - Open Trash"
)

choice=$(printf '%s\n' "${options[@]}" | ${DMENU} -l 2 -p 'Trash ') "$@"
if [[ $choice ]]; then
    if [[ $choice = "1 - Clean Trash" ]]; then
        trinst="$(pacman -Qq "trash-cli")"
        if [[ "$trinst" = "trash-cli" ]]; then
            trlist="$(trash-list)"
                if [[ "$trlist" = "" ]]; then
                   notify-send "Trash" "Empty trash. Nothing to purge." --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/places/user-trash-symbolic.svg" && exit 0
                else
                   trash-empty && notify-send "Trash" "Trash successfully cleaned." --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/places/user-trash-symbolic.svg" && exit 0
                fi
        else
            notify-send "'trash-cli' Is Not Installed." "Please verify and try again." --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/process-error-symbolic.svg" && exit 1
        fi
    elif [[ $choice = "2 - Open Trash" ]]; then
        nohup $filebrowser "trash:///" >/dev/null 2>&1 &
    else
        $scriptpath/$scriptname && exit 0
    fi
else
    echo "User chose to cancel. Program Terminated." && exit 0
fi  
