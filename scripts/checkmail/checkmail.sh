#!/bin/bash
##  _   _  _  _      _                      _ 
## | | | |(_)| |    (_) _ __   _   _ __  __| | 
## | |_| || || |    | || '_ \ | | | |\ \/ /| | 
## |  _  || || |___ | || | | || |_| | >  < |_| 
## |_| |_||_||_____||_||_| |_| \__,_|/_/\_\(_) 
## 
##                     https://hilinux.com.br

source $HOME/.scripts/_dmenu/dmenu-colors;

scriptpath="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd )"
scriptname="checkmail.sh"

time="$(date +%a), $(date +%b) $(date +%d) | $(date +%H:%M:%S)"

declare -a acc=(
  "1 - All Accounts"
  "2 - Junior SMS"
  "3 - Altis"
  "4 - AltisJr"
  "5 - All Altis"
  "6 - Renaissance"
)

choice=$(printf '%s\n' "${acc[@]}" | ${DMENU} -l 6 -p 'Check/Sync Mail ');
if [[ -n $choice ]]; then

    ### Check if Mbsync is installed
    isyncinst="$(pacman -Qi isync | awk 'NR==1{print $3}')"
    if [ $isyncinst = "isync" ]; then

          if [[ $choice == "1 - All Accounts" ]]; then
              
              account="All Mail Accounts"
              notify-send "Checking $account" "This may take a while. Please wait...\n\n$time" \
                  --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/mail-unread-symbolic.svg"
              
              ### Run Mbsync ##################################
              killall mbsync &>/dev/null   # do not duplicate
              sleep 1
              run=$(mbsync -a 2>&1)
              ### Test if Mbsync succeed
              if [ $? -eq 1 ]; then
                notify-send "Mail Synchronization Failed" "\n\nError Message(s):\n\n$run\n\n$time" \
                    --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 1
              fi
              #################################################

              ### Verify old messages
              maildirold="/media/SandiskRAID/myMail/*/INBOX/cur/"
              old="$(find $maildirold -type f | wc -l)"
              maildiroldrenaissance="/media/SandiskRAID/myMail/renaissance/Inbox/cur/"
              oldrenaissance="$(find $maildiroldrenaissance -type f | wc -l)"
              oldtotal="$(expr $old + $oldrenaissance)"
              
              ### Verify new current status (after sync)
              checksynced="/media/SandiskRAID/myMail/*/INBOX/new/"
              newsynced="$(find $checksynced -type f | wc -l)"
              checksyncedrenaissance="/media/SandiskRAID/myMail/renaissance/Inbox/new/"
              newsyncedrenaissance="$(find $checksyncedrenaissance -type f | wc -l)"
              syncedtotal="$(expr $newsynced + $newsyncedrenaissance)"

              grandtotal="$(expr $oldtotal + $newsynced)"

          elif [[ $choice == "2 - Junior SMS" ]]; then
              
              account="Junior SMS Mail Account"
              notify-send "Checking $account" "This may take a while. Please wait...\n\n$time" \
                  --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/mail-unread-symbolic.svg"

              ### Run Mbsync ##################################
              killall mbsync &>/dev/null   # do not duplicate
              sleep 1
              run=$(mbsync juniorsms 2>&1)
              ### Test if Mbsync succeed
              if [ $? -eq 1 ]; then
                notify-send "Mail Synchronization Failed" "\n\nError Message(s):\n\n$run\n\n$time" \
                    --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 1
              fi
              #################################################
              
              ### Verify old messages
              maildirold="/media/SandiskRAID/myMail/juniorsms/INBOX/cur/"
              old="$(find $maildirold -type f | wc -l)"
              oldtotal="$old"

              ### Verify new current status (after sync)
              checksynced="/media/SandiskRAID/myMail/juniorsms/INBOX/new/"
              newsynced="$(find $checksynced -type f | wc -l)"
              syncedtotal="$newsynced"

              grandtotal="$(expr $oldtotal + $newsynced)"

          elif [[ $choice == "3 - Altis" ]]; then
              
              account="Altis Mail Account"
              notify-send "Checking $account" "This may take a while. Please wait...\n\n$time" \
                  --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/mail-unread-symbolic.svg"

              ### Run Mbsync ##################################
              killall mbsync &>/dev/null   # do not duplicate
              sleep 1
              run=$(mbsync altis 2>&1)
              ### Test if Mbsync succeed
              if [ $? -eq 1 ]; then
                notify-send "Mail Synchronization Failed" "\n\nError Message(s):\n\n$run\n\n$time" \
                    --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 1
              fi
              #################################################
              
              ### Verify old messages
              maildirold="/media/SandiskRAID/myMail/altis/INBOX/cur/"
              old="$(find $maildirold -type f | wc -l)"
              oldtotal="$old"

              ### Verify new current status (after sync)
              checksynced="/media/SandiskRAID/myMail/altis/INBOX/new/"
              newsynced="$(find $checksynced -type f | wc -l)"
              syncedtotal="$newsynced"

              grandtotal="$(expr $oldtotal + $newsynced)"

          elif [[ $choice == "4 - AltisJr" ]]; then
              
              account="AltisJr Mail Account"
              notify-send "Checking $account" "This may take a while. Please wait...\n\n$time" \
                  --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/mail-unread-symbolic.svg"

              ### Run Mbsync ##################################
              killall mbsync &>/dev/null   # do not duplicate
              sleep 1
              run=$(mbsync altisjr 2>&1)
              ### Test if Mbsync succeed
              if [ $? -eq 1 ]; then
                notify-send "Mail Synchronization Failed" "\n\nError Message(s):\n\n$run\n\n$time" \
                    --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 1
              fi
              #################################################
              
              ### Verify old messages
              maildirold="/media/SandiskRAID/myMail/altisjr/INBOX/cur/"
              old="$(find $maildirold -type f | wc -l)"
              oldtotal="$old"

              ### Verify new current status (after sync)
              checksynced="/media/SandiskRAID/myMail/altisjr/INBOX/new/"
              newsynced="$(find $checksynced -type f | wc -l)"
              syncedtotal="$newsynced"

              grandtotal="$(expr $oldtotal + $newsynced)"

          elif [[ $choice == "5 - All Altis" ]]; then
              
              account="All Altis Mail Accounts"
              notify-send "Checking $account" "This may take a while. Please wait...\n\n$time" \
                  --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/mail-unread-symbolic.svg"

              ### Run Mbsync ##################################
              killall mbsync &>/dev/null   # do not duplicate
              sleep 1
              run=$(mbsync altis altisjr 2>&1)
              ### Test if Mbsync succeed
              if [ $? -eq 1 ]; then
                notify-send "Mail Synchronization Failed" "\n\nError Message(s):\n\n$run\n\n$time" \
                    --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 1
              fi
              #################################################
              
              ### Verify old messages
              maildirold_altis="/media/SandiskRAID/myMail/altis/INBOX/cur/"
              old_altis="$(find $maildirold_altis -type f | wc -l)"
              maildirold_altisjr="/media/SandiskRAID/myMail/altisjr/INBOX/cur/"
              old="$(find $maildirold_altisjr -type f | wc -l)"
              oldtotal="$(expr $old_altis + $old_altisjr)"

              ### Verify new current status (after sync)
              checksynced_altis="/media/SandiskRAID/myMail/altis/INBOX/new/"
              newsynced_altis="$(find $checksynced_altis -type f | wc -l)"
              checksynced_altisjr="/media/SandiskRAID/myMail/altisjr/INBOX/new/"
              newsynced_altisjr="$(find $checksynced_altisjr -type f | wc -l)"
              syncedtotal="$(expr $newsynced_altis + $newsynced_altisjr)"

              grandtotal="$(expr $oldtotal + $newsynced)"

          elif [[ $choice == "6 - Renaissance" ]]; then
              
              account="Renaissance Mail Account"
              notify-send "Checking $account" "This may take a while. Please wait...\n\n$time" \
                  --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/mail-unread-symbolic.svg"

              ### Run Mbsync ##################################
              killall mbsync &>/dev/null   # do not duplicate
              sleep 1
              run=$(mbsync renaissance 2>&1)
              ### Test if Mbsync succeed
              if [ $? -eq 1 ]; then
                notify-send "Mail Synchronization Failed" "\n\nError Message(s):\n\n$run\n\n$time" \
                    --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 1
              fi
              #################################################
              
              ### Verify old messages
              maildirold="/media/SandiskRAID/myMail/renaissance/Inbox/cur/"
              old="$(find $maildirold -type f | wc -l)"
              oldtotal="$old"

              ### Verify new current status (after sync)
              checksynced="/media/SandiskRAID/myMail/renaissance/Inbox/new/"
              newsynced="$(find $checksynced -type f | wc -l)"
              syncedtotal="$newsynced"

              grandtotal="$(expr $oldtotal + $newsynced)"

          else
              # notify-send "Invalid Mail Account(s)" "You chose an invalid mail account(s).\nPlease try again.\n\n$time" \
                 # --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg"
              # echo "User chose an invalid account." && exit 1
              $scriptpath/$scriptname && exit 0
          fi

      if [ $syncedtotal -gt 0 ]; then
            notify-send "You Have New Mail!" "$account\nNew: $syncedtotal | Old: $oldtotal | Total: $grandtotal\n\n$time" \
                --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/mail-important-symbolic.svg" && exit 0
      else
            notify-send "You Have No New Mail" "$account\nNew: $syncedtotal | Old: $oldtotal | Total: $grandtotal\n\n$time" \
                --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/mail-read-symbolic.svg" && exit 0
      fi
    else
        notify-send "Mbsync Is Not Installed" "Install it and setup your mail account(s).\n\n$time" \
          --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 1
    fi
else
    echo "User chose not to check mail." && exit 0
fi  
