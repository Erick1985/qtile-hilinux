#!/bin/bash
##  _   _  _  _      _                      _ 
## | | | |(_)| |    (_) _ __   _   _ __  __| | 
## | |_| || || |    | || '_ \ | | | |\ \/ /| | 
## |  _  || || |___ | || | | || |_| | >  < |_| 
## |_| |_||_||_____||_||_| |_| \__,_|/_/\_\(_) 
## 
##                     https://hilinux.com.br

time="$(date +%a), $(date +%b) $(date +%d) | $(date +%H:%M:%S)"

### Check if Mbsync is installed
isyncinst="$(pacman -Qi isync | awk 'NR==1{print $3}')"
if [ $isyncinst = "isync" ]; then

    account="All Mail Accounts"
    ### Run Mbsync ##################################
    killall mbsync &>/dev/null   # do not duplicate
    sleep 1
    run=$(mbsync -a 2>&1)
    ### Test if Mbsync succeed
    if [ $? -eq 1 ]; then
      DISPLAY=:0 DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus setpriv --euid=1000 \
        notify-send "Mail Synchronization Failed" "Error Message(s):\n\n$run\n\n$time" \
          --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 1
    fi
    #################################################

    ### Verify old messages
    maildirold="/media/SandiskRAID/myMail/*/INBOX/cur/"
    old="$(find $maildirold -type f | wc -l)"
    maildiroldrenaissance="/media/SandiskRAID/myMail/renaissance/Inbox/cur/"
    oldrenaissance="$(find $maildiroldrenaissance -type f | wc -l)"
    oldtotal="$(expr $old + $oldrenaissance)"

    ### Verify current status (after sync)
    checksynced="/media/SandiskRAID/myMail/*/INBOX/new/"
    newsynced="$(find $checksynced -type f | wc -l)"
    checksyncedrenaissance="/media/SandiskRAID/myMail/renaissance/Inbox/new/"
    newsyncedrenaissance="$(find $checksyncedrenaissance -type f | wc -l)"
    syncedtotal="$(expr $newsynced + $newsyncedrenaissance)"

    grandtotal="$(expr $oldtotal + $syncedtotal)"

    if [ $syncedtotal -gt 0 ]; then
          DISPLAY=:0 DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus setpriv --euid=1000 \
            notify-send "You Have New Mail!" "$account\nNew: $syncedtotal | Old: $oldtotal | Total: $grandtotal\n\n$time" \
              --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/mail-important-symbolic.svg" && exit 0
    fi

else
  DISPLAY=:0 DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus setpriv --euid=1000 \
      notify-send "Mail Synchronization Failed" "iSync is not installed on your system.\n\n$time" \
        --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 1
fi
