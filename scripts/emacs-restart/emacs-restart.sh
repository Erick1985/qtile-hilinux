#!/bin/bash
##  _   _  _  _      _                      _ 
## | | | |(_)| |    (_) _ __   _   _ __  __| | 
## | |_| || || |    | || '_ \ | | | |\ \/ /| | 
## |  _  || || |___ | || | | || |_| | >  < |_| 
## |_| |_||_||_____||_||_| |_| \__,_|/_/\_\(_) 
## 
##                     https://hilinux.com.br

emacspid=$(ps --user "$(id -u)" w | grep -e "emacs --daemon" | grep -e "Ssl" | awk '{print $1}')
if [ -z "$emacspid" ]; then
    /usr/bin/emacs --daemon
    echo ""
    echo ">>> Emacs Daemon wasn't running, so it was STARTED. <<<"
else
    kill -9 $emacspid && /usr/bin/emacs --daemon
    echo ""
    echo ">>> Emacs Daemon was running, so it was RESTARTED. <<<"
fi
