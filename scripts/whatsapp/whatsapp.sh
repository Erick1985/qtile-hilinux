#!/bin/bash
##  _   _  _  _      _                      _ 
## | | | |(_)| |    (_) _ __   _   _ __  __| | 
## | |_| || || |    | || '_ \ | | | |\ \/ /| | 
## |  _  || || |___ | || | | || |_| | >  < |_| 
## |_| |_||_||_____||_||_| |_| \__,_|/_/\_\(_) 
## 
##                     https://hilinux.com.br

source $HOME/.scripts/_dmenu/dmenu-colors;

if [ ! -d ~/.WebApps ]; then
    mkdir ~/.WebApps && cd ~/.WebApps || exit 1
fi

scriptpath="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd )"
scriptname="$( ls $scriptpath | grep sh )"

whatsapp() {
nativefier "web.whatsapp.com" -n "WhatsApp" -u "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/605.1.15 (KHTML, like Gecko) Chrome/94.0.4606.61 Safari/605.1.15" && \
notify-send "WhatsApp" "WhatsApp successful installed." \
    --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/process-completed-symbolic.svg" && exit 0 || \
notify-send "WhatsApp" "Error installing WhatsApp." \
    --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 1
}

whatsapp_business() {
nativefier "web.whatsapp.com" -n "WhatsAppBusiness" -u "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/605.1.15 (KHTML, like Gecko) Chrome/94.0.4606.61 Safari/605.1.15" && \
notify-send "WhatsApp Business" "WhatsApp Business successful installed." \
    --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/process-completed-symbolic.svg" && exit 0 || \
notify-send "WhatsApp Business" "Error installing WhatsApp Business." \
    --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 1
}

declare -a wapp=(
  "1 - WhatsApp"
  "2 - WhatsApp Business"
)

choice=$(printf '%s\n' "${wapp[@]}" | ${DMENU} -l 5 -p '(Re) Install ')
if [ "$choice" ]; then 
    cd ~/.WebApps
    if [[ $choice == "1 - WhatsApp" ]]; then 
        if [[ $(find . -mindepth 1 -type d -name 'WhatsApp-*' | wc -l) -gt 0 ]]; then
            if [[ "$(echo -e "No\nYes" | ${DMENU} -p "Overwrite? ")" == "Yes" ]]; then
                find . -mindepth 1 -type d -name 'WhatsApp-*' -exec rm -rf '{}' \; -prune
                whatsapp
            else
                echo "User chose not to overwrite." && exit 0
            fi
        else
            whatsapp
        fi
    elif [[ $choice == "2 - WhatsApp Business" ]]; then 
        if [[ $(find . -mindepth 1 -type d -name 'WhatsAppBusiness-*' | wc -l) -gt 0 ]]; then
            if [[ "$(echo -e "No\nYes" | ${DMENU} -p "Overwrite? ")" == "Yes" ]]; then
                find . -mindepth 1 -type d -name 'WhatsAppBusiness-*' -exec rm -rf '{}' \; -prune
                whatsapp_business
            else
                echo "User chose not to overwrite." && exit 0
            fi
        else
            whatsapp_business
        fi
    else
        $scriptpath/$scriptname && exit 0
    fi
else
    echo "User chose to quit." && exit 0
fi 
