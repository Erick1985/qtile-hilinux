#!/bin/bash
##  _   _  _  _      _                      _ 
## | | | |(_)| |    (_) _ __   _   _ __  __| | 
## | |_| || || |    | || '_ \ | | | |\ \/ /| | 
## |  _  || || |___ | || | | || |_| | >  < |_| 
## |_| |_||_||_____||_||_| |_| \__,_|/_/\_\(_) 
## 
##                     https://hilinux.com.br

source $HOME/.scripts/_dmenu/dmenu-colors;

declare -a scripts=(
    "Directory Bookmarks"
    "Dmenu Colors"
    "Bookmarks"
    "Bookmarks Edit"
    "Web Search"
    "DM Logout"
    "DM Music"
    "MPC Music"
    "DotFiles Edit"
    "FreeFileSync"
    "Kill Process"
    "Mail Check"
    "CalCont Sync"
    "Messengers"
    "Manual Pages"
    "Pass Menu"
    "Qutebrowser Cleaner"
    "Keybinding Helper"
    "Scripts Edit"
    "Scripts Permissions"
    "Screenshot"
    "Dotfiles+ Backup"
    "Trash Empty"
    "Time Capsule"
    "WhatsApp"
    "YouTube"
)

declare -a winman=(
    "Qtile"
    "Xmonad"
)

declare -a editor=(
    "Neovim"
    "Emacs"
    "Gedit"
    "Leafpad"
)

choice=$(printf '%s\n' "${scripts[@]}" | sort -b -d -f | ${DMENU} -l 10 -p 'Edit Script ')
  if [ "$choice" ]; then
    if [[ "$choice" == "Directory Bookmarks" ]]; then
      scr=~/.scripts/bm-dirs/bm-dirs.sh
    elif [[ "$choice" == "Dmenu Colors" ]]; then
      scr=~/.scripts/_dmenu/dmenu-colors
    elif [[ "$choice" == "Bookmarks" ]]; then
      scr=~/.scripts/bookmarks/bookmarks.sh
    elif [[ "$choice" == "Bookmarks Edit" ]]; then
      scr=~/.scripts/bookmarks-edit/bookmarks-edit.sh
    elif [[ "$choice" == "Web Search" ]]; then
      scr=~/.scripts/web-search/web-search.sh
    elif [[ "$choice" == "DM Logout" ]]; then
      scr=~/.scripts/dmlogout/dmlogout.sh
    elif [[ "$choice" == "DM Music" ]]; then
      scr=~/.scripts/dmusic/dmusic.sh
    elif [[ "$choice" == "MPC Music" ]]; then
      scr=~/.scripts/dmusic/mpd.sh
    elif [[ "$choice" == "DotFiles Edit" ]]; then
      scr=~/.scripts/dotfiles/dotfiles.sh
    elif [[ "$choice" == "FreeFileSync" ]]; then
      scr=~/.scripts/ffs/ffs.sh
    elif [[ "$choice" == "Kill Process" ]]; then
      scr=~/.scripts/killproc/killproc.sh
    elif [[ "$choice" == "Messengers" ]]; then
      scr=~/.scripts/messengers/messengers.sh
    elif [[ "$choice" == "Manual Pages" ]]; then
      scr=~/.scripts/manpages/man.sh
    elif [[ "$choice" == "Mail Check" ]]; then
      scr=~/.scripts/manpages/man.sh
    elif [[ "$choice" == "CalCont Sync" ]]; then
      scr=~/.scripts/checkcalcont/checkcalcont.sh
    elif [[ "$choice" == "Pass Menu" ]]; then
      scr=~/.scripts/passmenu/passmenu.sh
    elif [[ "$choice" == "Qutebrowser Cleaner" ]]; then
      scr=~/.scripts/qutebrowser-cleaner/qutebrowser-cleaner.sh
    elif [[ "$choice" == "Keybinding Helper" ]]; then
        winman=$(printf '%s\n' "${winman[@]}" | ${DMENU} -l 4 -p 'Wich Window Manager? ')
        if [ "$winman" ]; then
            if [[ "$winman" == "Qtile" ]]; then
                scr=~/.config/qtile/scripts/qtile_keys.sh
            elif [[ "$winman" == "Xmonad" ]]; then
                scr=~/.xmonad/scripts/xmonad_keys.sh
            else
                echo "Program Terminated." && exit 1
            fi
        else
            echo "Program Terminated." && exit 1
        fi
    elif [[ "$choice" == "Scripts Edit" ]]; then
      scr=~/.scripts/scriptedit/scriptedit.sh
    elif [[ "$choice" == "Scripts Permissions" ]]; then
      scr=~/.scripts/setperm/setperm.sh
    elif [[ "$choice" == "Screenshot" ]]; then
      scr=~/.scripts/screenshot/screenshot
    elif [[ "$choice" == "Dotfiles+ Backup" ]]; then
      scr=~/.scripts/wmbkp/wmbkp.sh
    elif [[ "$choice" == "Trash Empty" ]]; then
      scr=~/.scripts/trash-empty/trash-empty.sh
    elif [[ "$choice" == "Time Capsule" ]]; then
      scr=~/.scripts/timecapsule/timecapsule
    elif [[ "$choice" == "WhatsApp" ]]; then
      scr=~/.scripts/whatsapp/whatsapp.sh
    elif [[ "$choice" == "YouTube" ]]; then
      scr=~/.scripts/yt-browser/yt-browser.sh
    else
        notify-send "ERROR" "Invalid script. Please try again." --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 1
    fi
  else
      echo "Program Terminated." && exit 1
  fi

  if [ -f $scr ]; then
      edtchoice=$(printf '%s\n' "${editor[@]}" | ${DMENU} -l 10 -p 'Choose Editor ')
      if [ "$edtchoice" ]; then
          if [[ $edtchoice = "Neovim" ]]; then
              edtpkg="neovim"
              cmd="st -e nvim"
          elif [[ $edtchoice = "Emacs" ]]; then
              edtpkg="emacs"
              cmd="emacsclient -c -a emacs"
          elif [[ $edtchoice = "Gedit" ]]; then
              edtpkg="gedit"
              cmd="gedit"
          elif [[ $edtchoice = "Leafpad" ]]; then
              edtpkg="leafpad"
              cmd="leafpad"
          else
              notify-send "ERROR" "Invalid Editor. Please try again." --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 1
          fi
      else
          echo "User choose to quit." && exit 1
      fi

      nvinst="$(pacman -Qq "$edtpkg")"
      if [[ $nvinst == "$edtpkg" ]]; then
          if [ "$scr" = "" ] || [ "$cmd" = "" ]; then
            notify-send "ERROR" "Invalid Script and/or Editor. Please try again." --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 1
          else
            nohup $cmd $scr > /dev/null 2>&1 &
          fi
      else
          notify-send "ERROR" "$edtchoice is not installed.\nInstall it or choose another one." --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 0
      fi
  else
    notify-send "ERROR" "$choice file does not exist.\nPlease verify." --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 0
  fi
