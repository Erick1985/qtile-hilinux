#!/bin/bash
##  _   _  _  _      _                      _ 
## | | | |(_)| |    (_) _ __   _   _ __  __| | 
## | |_| || || |    | || '_ \ | | | |\ \/ /| | 
## |  _  || || |___ | || | | || |_| | >  < |_| 
## |_| |_||_||_____||_||_| |_| \__,_|/_/\_\(_) 
## 
##                     https://hilinux.com.br

source $HOME/.scripts/_dmenu/dmenu-colors;

scriptpath="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd )"
scriptname="$( ls $scriptpath | grep sh )"

Directories_File="$HOME/.scripts/bm-dirs/bm-dirs"
[ ! -f $Directories_File ] && echo "Directories List File Missing." && exit 1

# File Browsers
gui_file_browser="thunar"
term_file_browser="ranger"
# Terminal
  term="st"
  termexec="-e"

declare -a fmb=(
  "Terminal"
  "Ranger"
  "Vifm"
  "Thunar"
)

bmdirs=$(awk '{for (i=2; i<NF; i++) printf $i" "; if (NF >= 2) print $1"    -    "$NF}' "$Directories_File") 
list="$(printf '%s\n' "${bmdirs}" | sort -b -d -f)"
list_format=$( printf '%s\n' "${list}" | sed '/^[[:space:]]*$/d' )
choice="$( echo -e "$list_format" | ${DMENU} -l 10 -p 'Directories ' "$@")"
[ ! -n "$choice" ] && echo "User chose to cancel. Program terminated." && exit 0
if [[ "$list_format" =~ "$choice" ]]; then
    dir="$(echo "${choice}" | awk '{print $NF}' | sed "s|~|$HOME|g")" || exit 1
    [ ! -d $dir ] && notify-send "Directory Does Not Exist" "Chosed directory is in your list, but\nit does not exist.\n\nPlease check your directory bookmarks file." \
        --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 0
    export VIFMDIR="$dir"
    dest=$(printf '%s\n' "${fmb[@]}" | ${DMENU} -l 5 -p 'Which File Browser? ')
    [ ! "$dest" ] && echo "User chose to cancel. Program Terminated." && exit 0
    if [[ "$dest" == "Terminal" ]]; then
      FBROWSER="$term -d"
    elif [[ "$dest" == "Ranger" ]]; then
      FBROWSER="$term $termexec $term_file_browser"
    elif [[ "$dest" == "Vifm" ]]; then
      FBROWSER="$term $termexec vifmrun"
    elif [[ "$dest" == "Thunar" ]]; then
      FBROWSER=$gui_file_browser
    fi
    if [ "$FBROWSER" = "$term -d" ] || [ "$FBROWSER" = "$term -e $term_file_browser" ] || [ "$FBROWSER" = "$term -e vifmrun" ] || [ $FBROWSER = $gui_file_browser ]; then
        nohup $FBROWSER $dir >/dev/null 2>&1 &
        export VIFMDIR="$HOME"
    else
        $scriptpath/$scriptname && exit 0
    fi
else
  $scriptpath/$scriptname && exit 0
fi
