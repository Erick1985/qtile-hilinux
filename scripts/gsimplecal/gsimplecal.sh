#!/bin/bash
##  _   _  _  _      _                      _ 
## | | | |(_)| |    (_) _ __   _   _ __  __| | 
## | |_| || || |    | || '_ \ | | | |\ \/ /| | 
## |  _  || || |___ | || | | || |_| | >  < |_| 
## |_| |_||_||_____||_||_| |_| \__,_|/_/\_\(_) 
## 
##                     https://hilinux.com.br

scriptpath="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd )"

# Check GSimpleCal Config Dir
GS=$HOME/.config/gsimplecal
[ ! -d $GS ] && mkdir ~/.config/gsimplecal

# Set GSimpleCal Config per Window Manager
if [[ "$DESKTOP_SESSION" == "qtile" ]]; then
    cp $scriptpath/config-qtile $GS/config && exit 0
elif [[ "$DESKTOP_SESSION" == "xmonad" ]]; then
    cp $scriptpath/config-xmonad $GS/config && exit 0
elif [[ "$DESKTOP_SESSION" == "dwm" ]]; then
    cp $scriptpath/config-dwm $GS/config && exit 0
elif [[ "$DESKTOP_SESSION" == "bspwm" ]]; then
    cp $scriptpath/config-bspwm $GS/config && exit 0
else
    [ -f "$GS/config" ] && rm $GS/config && exit 0
fi
