#!/bin/bash
##  _   _  _  _      _                      _ 
## | | | |(_)| |    (_) _ __   _   _ __  __| | 
## | |_| || || |    | || '_ \ | | | |\ \/ /| | 
## |  _  || || |___ | || | | || |_| | >  < |_| 
## |_| |_||_||_____||_||_| |_| \__,_|/_/\_\(_) 
## 
##                     https://hilinux.com.br

source $HOME/.scripts/_dmenu/dmenu-colors;

# Parameters
now=$(date '+%Y.%m.%d_%H-%M-%S')
nowstr=$(echo $now | sed 's/_/ | /g' | sed 's/\./\//g' | sed 's/-/:/g')
megadisk="SandiskRAID"
bkpname="AMD"
drive="/media/HDDRAID2T"
path="$drive/Softwares/Linux/WMs/$bkpname"
log="$drive/Softwares/Linux/WMs/bkplog.txt"
bkpfull=0

if [[ "$(echo -e "No\nYes" | ${DMENU} -p "Perform a Config Backup? ")" == "Yes" ]]; then
  if ! mountpoint -q -- $drive; then  
      printf "[$nowstr] BACKUP ERROR - Target path does not exist.\n" >> $log
      notify-send "Backup Error" "Target path does not exist.\nPlease verify mountpoint." --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" 
  else
    mkdir ~/.wmbkp
    mkdir ~/.wmbkp/neomutt-apps
    let "bkpfull=bkpfull + 1"
    ### Packages List (Pacman and AUR) #############
    pacman -Qneq > ~/.wmbkp/pacman-Qneq.txt
    let "bkpfull=bkpfull + 1"
    pacman -Qmeq > ~/.wmbkp/pacman-Qmeq.txt
    let "bkpfull=bkpfull + 1"
    ### Home Dotfiles ##############################
    cp -r ~/.abook/ ~/.wmbkp
    let "bkpfull=bkpfull + 1"
    cp -r ~/.password-store/ ~/.wmbkp
    let "bkpfull=bkpfull + 1"
    cp -r ~/.bashrc ~/.wmbkp
    let "bkpfull=bkpfull + 1"
    cp -r ~/.imwheelrc ~/.wmbkp
    let "bkpfull=bkpfull + 1"
    cp ~/.mbsyncrc ~/.wmbkp/neomutt-apps/
    cp ~/.msmtprc ~/.wmbkp/neomutt-apps/
    cp ~/.urlview ~/.wmbkp/neomutt-apps/
    cp ~/.xbindkeysrc ~/.wmbkp/
    let "bkpfull=bkpfull + 1"
    cp -r ~/.Xresources ~/.wmbkp
    let "bkpfull=bkpfull + 1"
    cp -r ~/.zshrc ~/.wmbkp
    let "bkpfull=bkpfull + 1"
    cp -r ~/.doom.d ~/.wmbkp
    let "bkpfull=bkpfull + 1"
    cp -r ~/.moc ~/.wmbkp
    let "bkpfull=bkpfull + 1"
    cp -r ~/.scripts ~/.wmbkp
    let "bkpfull=bkpfull + 1"
    cp -r ~/.sources ~/.wmbkp
    let "bkpfull=bkpfull + 1"
    cp -r ~/.xmonad ~/.wmbkp
    let "bkpfull=bkpfull + 1"
    ### Config Folder ##############################
    cp -r ~/.config/alacritty/ ~/.wmbkp
    let "bkpfull=bkpfull + 1"
    cp -r ~/.config/bspwm/ ~/.wmbkp
    let "bkpfull=bkpfull + 1"
    cp -r ~/.config/calcurse ~/.wmbkp
    let "bkpfull=bkpfull + 1"
    cp -r ~/.config/htop ~/.wmbkp
    let "bkpfull=bkpfull + 1"
    cp -r ~/.config/i3lock ~/.wmbkp
    let "bkpfull=bkpfull + 1"
    cp -r ~/.config/khard ~/.wmbkp
    let "bkpfull=bkpfull + 1"
    cp -r ~/.config/mpd ~/.wmbkp
    let "bkpfull=bkpfull + 1"
    cp -r ~/.config/ncmpcpp ~/.wmbkp
    let "bkpfull=bkpfull + 1"
    cp -r ~/.config/neomutt ~/.wmbkp
    let "bkpfull=bkpfull + 1"
    cp -r ~/.config/nvim ~/.wmbkp
    let "bkpfull=bkpfull + 1"
    cp -r ~/.config/polybar ~/.wmbkp
    let "bkpfull=bkpfull + 1"
    cp -r ~/.config/qtile ~/.wmbkp
    let "bkpfull=bkpfull + 1"
    cp -r ~/.config/qutebrowser ~/.wmbkp
    let "bkpfull=bkpfull + 1"
    cp -r ~/.config/ranger ~/.wmbkp
    let "bkpfull=bkpfull + 1"
    cp -r ~/.config/rofi ~/.wmbkp
    let "bkpfull=bkpfull + 1"
    cp -r ~/.config/sxhkd/ ~/.wmbkp
    let "bkpfull=bkpfull + 1"
    cp -r ~/.config/sxiv ~/.wmbkp
    let "bkpfull=bkpfull + 1"
    cp -r ~/.config/vdirsyncer ~/.wmbkp
    let "bkpfull=bkpfull + 1"
    cp -r ~/.config/vifm ~/.wmbkp
    let "bkpfull=bkpfull + 1"
    cp -r ~/.config/xmobar ~/.wmbkp
    let "bkpfull=bkpfull + 1"
    cp -r ~/.config/zathura ~/.wmbkp
    let "bkpfull=bkpfull + 1"
    ### Local User Folder ##########################
    cp -r ~/.local/bin/ ~/.wmbkp/localbin
    let "bkpfull=bkpfull + 1"
    ### RAID Driver ################################
    cp -r $drive/Spotify/.config ~/.wmbkp/ncspot
    let "bkpfull=bkpfull + 1"
    cd ~/
    count=$(ls -a ~/.wmbkp | wc -l)
    bkpcount="$(expr $count - 2)"
    if [[ "$bkpcount" = "$bkpfull" ]]; then
        mv .wmbkp WM-Configs-$bkpname
        tar -czSpf $path/"WM-Configs-$bkpname[$now].tar.gz" WM-Configs-$bkpname
        rm -rf ~/WM-Configs-$bkpname
        delnum="$( find $path -mtime +3 | wc -l )"
        if [ $delnum = "0" ]; then
            del="No"
        else
            del="$delnum"
            find $path -mtime +3 -delete
        fi
        printf "[$nowstr] SUCCESSFUL BACKUP [$del Old Backup(s) Deleted (3d+)]\n" >> $log
        notify-send "Dotfiles+ Backup" "Successful backup at $nowstr\nin $drive.\n$del Old Backup(s) Deleted (3d+)" \
            --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/process-completed-symbolic.svg" && exit 0
    elif [[ "$bkpcount" = "0" ]]; then
        rm -rf ~/.wmbkp
        printf "[$nowstr] BACKUP ERROR - No Backup Done [No Old Backup(s) Deleted (3d+)]\n" >> $log
        notify-send "Dotfiles+ Backup" "No Backup Done at $nowstr.\nNo Old Backup(s) Deleted (3d+)" \
            --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 0
    elif [[ "$bkpcount" -gt "$bkpfull" ]]; then
        mv .wmbkp WM-Configs-$bkpname
        tar -czSpf $path/"WM-Configs-$bkpname[$now].tar.gz" WM-Configs-$bkpname
        rm -rf ~/WM-Configs-$bkpname
        printf "[$nowstr] BACKUP ALERT - Over Backup ($bkpcount/$bkpfull) [No Old Backup(s) Deleted (3d+)]\n" >> $log
        notify-send "Dotfiles+ Backup" "Over Backup ($bkpcount/$bkpfull) at $nowstr\nin $drive.\nNo Old Backup(s) Deleted (3d+)\nPlease verify your backup." \
            --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-warning-symbolic.svg" && exit 1
    else
        mv .wmbkp WM-Configs-$bkpname
        tar -czSpf $path/"WM-Configs-$bkpname[$now].tar.gz" WM-Configs-$bkpname
        rm -rf ~/WM-Configs-$bkpname
        printf "[$nowstr] BACKUP ALERT - Partial Backup ($bkpcount/$bkpfull) [No Old Backup(s) Deleted (3d+)]\n" >> $log
        notify-send "Dotfiles+ Backup" "Partial Backup ($bkpcount/$bkpfull) at $nowstr\nin $drive.\nNo Old Backup(s) Deleted (3d+)\nPlease verify your backup." \
            --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-warning-symbolic.svg" && exit 1
    fi
  fi
else
  echo "User chose not to backup." && exit 0
fi
