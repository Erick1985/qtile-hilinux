#!/bin/bash
##  _   _  _  _      _                      _ 
## | | | |(_)| |    (_) _ __   _   _ __  __| | 
## | |_| || || |    | || '_ \ | | | |\ \/ /| | 
## |  _  || || |___ | || | | || |_| | >  < |_| 
## |_| |_||_||_____||_||_| |_| \__,_|/_/\_\(_) 
## 
##                     https://hilinux.com.br

source $HOME/.scripts/_dmenu/dmenu-colors;

scriptpath="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd )"
scriptname="$( ls $scriptpath )"

declare -a browsers=(
  "Qutebrowser"
  "Brave Browser"
  "Mozilla Firefox"
)

declare -a engines=(
  # Search Engines
  "Bing   -   https://www.bing.com/search?q="
  "Brave   -   https://search.brave.com/search?q="
  "DuckDuckGo   -   https://duckduckgo.com/?q="
  "Google   -   https://www.google.com/search?q="
  "Qwant   -   https://www.qwant.com/?q="
  "SwissCows   -   https://swisscows.com/web?query="
  "Yandex   -   https://yandex.com/search/?text="
    "Startpage   -   https://www.startpage.com/do/dsearch?query="
  # Information/News
  "CNN   -   https://www.cnn.com.br/search?q="
  "GoogleNews   -   https://news.google.com.br/search?q="
  "Wikipedia   -   https://en.wikipedia.org/w/index.php?search="
  "Wiktionary   -   https://en.wiktionary.org/w/index.php?search="
  # Social Media
  "Reddit   -   https://www.reddit.com/search/?q="
  "Odysee   -   https://odysee.com/$/search?q="
  "YouTube   -   https://www.youtube.com/results?search_query="
  # Online Shopping
  "Amazon   -   https://www.amazon.com.br/s?k="
  "eBay   -   https://www.ebay.com/sch/i.html?&_nkw="
    "MercadoLivre   -   https://lista.mercadolivre.com.br/"
  "Zoom   -   https://zoom.com.br/search?q="
  # Linux
  "ArchAUR   -   https://aur.archlinux.org/packages/?O=0&K="
  "ArchPkg   -   https://archlinux.org/packages/?sort=&q="
  "ArchWiki   -   https://wiki.archlinux.org/index.php?search="
  "DebianPkg   -   https://packages.debian.org/search?suite=default&section=all&arch=any&searchon=names&keywords="
  # Development
  "GitHub   -   https://github.com/search?q="
  "GitLab    -   https://gitlab.com/search?search="
  "GoogleOpenSource   -   https://opensource.google/projects/search?q="
  "SourceForge    -   https://sourceforge.net/directory/?q="
  "StackOverflow   -   https://stackoverflow.com/search?q="
)
choice=$(printf '%s\n' "${engines[@]}" | sort -b -d -f | ${DMENU} -l 10 -p 'Choose Search Engine ') "$@"
if [[ "$choice" == "" ]]; then
	echo "No Search Engine. Program Terminated." && exit 1
elif [[ "$choice" ]]; then
    if [[ "$choice" = "YouTube   -   https://www.youtube.com/results?search_query=" ]]; then
        BROWSER="chromium"
            url=$(echo "$choice" | awk '{ print $NF }') || exit 1
            query=$(echo "$choice" | ${DMENU} -l 4 -p 'Enter search query:')
            query="$(echo "${query}")"
            if [ "$query" ]; then
                if [ "$query" = "$choice" ]; then
                  notify-send "No Query Entry" "Please try again." --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/process-error-symbolic.svg" && exit 1
                else
                  nohup "$BROWSER" "${url}${query}" > /dev/null 2>&1 &
                fi   
            else
                echo "No query. Program Terminated." && exit 1
            fi
    elif [[ "${engines[@]}" =~ "$choice" ]]; then
        browser=$(printf '%s\n' "${browsers[@]}" | ${DMENU} -l 4 -p 'Which Browser? ')
          if [ "$browser" ]; then
            if [[ "$browser" == "Qutebrowser" ]]; then
              BROWSER="qutebrowser"
            elif [[ "$browser" == "Brave Browser" ]]; then
              BROWSER="brave"
            elif [[ "$browser" == "Mozilla Firefox" ]]; then
              BROWSER="firefox"
            else
              notify-send "Invalid Browser" "Please try again." --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/process-error-symbolic.svg" && exit 1
            fi
            url=$(echo "$choice" | awk '{ print $NF }') || exit 1
            query=$(echo "$choice" | ${DMENU} -l 4 -p 'Enter search query:')
            query="$(echo "${query}")"
            if [ "$query" ]; then
                if [ "$query" = "$choice" ]; then
                  notify-send "No Search Query" "Please try again." --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/process-error-symbolic.svg" && exit 1
                else
                  nohup "$BROWSER" "${url}${query}" > /dev/null 2>&1 &
                fi   
            else
                echo "No query. Program Terminated." && exit 1
            fi
          else
            echo "No browser. Program Terminated." && exit 1
          fi
    else
       $scriptpath/$scriptname && exit 0
    fi
else
  echo "User chose to quit." && exit 1
fi
