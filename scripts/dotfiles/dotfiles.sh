#!/bin/bash
##  _   _  _  _      _                      _ 
## | | | |(_)| |    (_) _ __   _   _ __  __| | 
## | |_| || || |    | || '_ \ | | | |\ \/ /| | 
## |  _  || || |___ | || | | || |_| | >  < |_| 
## |_| |_||_||_____||_||_| |_| \__,_|/_/\_\(_) 
## 
##                     https://hilinux.com.br

source $HOME/.scripts/_dmenu/dmenu-colors;

declare -a dotfiles=(
    "bashrc"
    "neomutt"
    "alacritty"
    "dwm startup"
    "dwm"
    "dwmblocks"
    "bspwm"
    "bspwm startup"
    "sxhkd"
    "emacs"
    "moc"
    "ncspot"
    "ncmpcpp"
    "nvim"
    "polybar"
    "qtile startup"
    "qtile"
    "qutebrowser"
    "ranger"
    "st terminal"
    "sxiv"
    "vifm"
    "xbindkeys"
    "xmobar"
    "xmonad startup"
    "xmonad"
    "Xresources"
    "zshrc"
)

choice=$(printf '%s\n' "${dotfiles[@]}" | sort -b -d -f | ${DMENU} -l 10 -p 'Edit Dotfile ')
  if [ "$choice" ]; then
    if [[ "$choice" == "bashrc" ]]; then
      dotf=~/.bashrc
    elif [[ "$choice" == "neomutt" ]]; then
      dotf=~/.config/neomutt/settings
    elif [[ "$choice" == "alacritty" ]]; then
      dotf=~/.config/alacritty/alacritty.yml
    elif [[ "$choice" == "moc" ]]; then
      dotf=~/.moc/config
    elif [[ "$choice" == "ncspot" ]]; then
      dotf=/media/HDDRAID2T/Spotify/.config/config.toml
    elif [[ "$choice" == "ncmpcpp" ]]; then
      dotf=~/.config/ncmpcpp/config
    elif [[ "$choice" == "zshrc" ]]; then
      dotf=~/.zshrc
    elif [[ "$choice" == "Xresources" ]]; then
      dotf=~/.Xresources
    elif [[ "$choice" == "nvim" ]]; then
      dotf=~/.config/nvim/init.vim
    elif [[ "$choice" == "qtile" ]]; then
      dotf=~/.config/qtile/README.org
    elif [[ "$choice" == "qtile startup" ]]; then
      dotf=~/.config/qtile/autostart.sh
    elif [[ "$choice" == "dwm" ]]; then
      dotf=~/.sources/dwm/config.def.h
    elif [[ "$choice" == "dwm startup" ]]; then
      dotf=~/.sources/dwm/autostart.sh
    elif [[ "$choice" == "dwmblocks" ]]; then
      dotf=~/.sources/dwmblocks/blocks.def.h
    elif [[ "$choice" == "bspwm" ]]; then
      dotf=~/.config/bspwm/bspwmrc
    elif [[ "$choice" == "bspwm startup" ]]; then
      dotf=~/.config/bspwm/autostart.sh
    elif [[ "$choice" == "sxhkd" ]]; then
      dotf=~/.config/sxhkd/sxhkdrc
    elif [[ "$choice" == "polybar" ]]; then
      dotf=~/.config/polybar/config
    elif [[ "$choice" == "st terminal" ]]; then
      dotf=~/.sources/st/config.h
    elif [[ "$choice" == "qutebrowser" ]]; then
      dotf=~/.config/qutebrowser/config.py
    elif [[ "$choice" == "ranger" ]]; then
      dotf=~/.config/ranger/rc.conf
    elif [[ "$choice" == "vifm" ]]; then
      dotf=~/.config/vifm/vifmrc
    elif [[ "$choice" == "sxiv" ]]; then
      dotf=~/.config/sxiv/exec/key-handler
    elif [[ "$choice" == "xmonad" ]]; then
      dotf=~/.xmonad/xmonad.hs
    elif [[ "$choice" == "xmonad startup" ]]; then
      dotf=~/.xmonad/autostart.sh
    elif [[ "$choice" == "xmobar" ]]; then
      dotf=~/.config/xmobar/xmobarrc1
    elif [[ "$choice" == "xbindkeys" ]]; then
      dotf=~/.xbindkeysrc
    elif [[ "$choice" == "emacs" ]]; then
      dotf=~/.doom.d/config.el
    else
        notify-send "Invalid DotFile" "Please try again." \
            --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 1
    fi
  else
    echo "Program Terminated." && exit 1
  fi

  declare -a editor=(
      "Neovim"
      "Emacs"
      "Gedit"
      "Leafpad"
  )

  if [ -f $dotf ]; then
    if [ $dotf = "$HOME/.config/qtile/README.org" ]; then
      edtpkg="emacs"
      cmd="emacsclient -c -a emacs"
    else
      edtchoice=$(printf '%s\n' "${editor[@]}" | ${DMENU} -l 10 -p 'Choose Editor ')
      if [ "$edtchoice" ]; then
          if [[ $edtchoice = "Neovim" ]]; then
              edtpkg="neovim"
              cmd="st -e nvim"
          elif [[ $edtchoice = "Emacs" ]]; then
              edtpkg="emacs"
              cmd="emacsclient -c -a emacs"
          elif [[ $edtchoice = "Gedit" ]]; then
              edtpkg="gedit"
              cmd="gedit"
          elif [[ $edtchoice = "Leafpad" ]]; then
              edtpkg="leafpad"
              cmd="leafpad"
          else
              notify-send "Invalid Editor" "Please try again." \
                  --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 1
          fi
      else
          echo "User choose to quit." && exit 1
      fi
    fi

      nvinst="$(pacman -Qq "$edtpkg")"
      if [[ $nvinst == "$edtpkg" ]]; then
          if [ "$dotf" = "" ] || [ "$cmd" = "" ]; then
            notify-send "Invalid Dotfile/Editor" "Please try again." \
                --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 1
          else
            nohup $cmd $dotf > /dev/null 2>&1 &
          fi
      else
          notify-send "$edtchoice Is Not Installed" "Install it or choose another one." \
              --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 0
      fi
  else
    notify-send "<$choice> Does Not Exist" "Please verify." \
        --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 0
  fi
