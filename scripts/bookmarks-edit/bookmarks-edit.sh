#!/bin/bash
##  _   _  _  _      _                      _ 
## | | | |(_)| |    (_) _ __   _   _ __  __| | 
## | |_| || || |    | || '_ \ | | | |\ \/ /| | 
## |  _  || || |___ | || | | || |_| | >  < |_| 
## |_| |_||_||_____||_||_| |_| \__,_|/_/\_\(_) 
## 
##                     https://hilinux.com.br

source $HOME/.scripts/_dmenu/dmenu-colors;

scriptpath="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd )"
scriptname="$( ls $scriptpath )"

declare -a bm=(
    "1 - Internet Bookmarks"
    "2 - Directory Bookmarks"
)

declare -a editor=(
    "Neovim"
    "Emacs"
    "Gedit"
    "Leafpad"
)

choice=$(printf '%s\n' "${bm[@]}" | ${DMENU} -l 2 -p 'Edit Bookmarks ');
if [[ -n $choice ]]; then
   if [[ "$choice" == "1 - Internet Bookmarks" ]]; then
     bmk=~/.config/qutebrowser/bookmarks/urls
   elif [[ "$choice" == "2 - Directory Bookmarks" ]]; then
     bmk=~/.scripts/bm-dirs/bm-dirs
   else
     $scriptpath/$scriptname && exit 0
   fi
else
   echo "User chose to quit. Program terminated." && exit 0
fi

if [ ! -f $bmk ]; then
    notify-send "$choice File Does Not Exist" "Please verify and try again." \
        --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 1
else
    edtchoice=$(printf '%s\n' "${editor[@]}" | ${DMENU} -l 10 -p 'Choose Editor ')
    if [ "$edtchoice" ]; then
        if [[ $edtchoice == "Neovim" ]]; then
            edtpkg="neovim"
            cmd="st -e nvim"
        elif [[ $edtchoice == "Emacs" ]]; then
            edtpkg="emacs"
            cmd="emacsclient -c -a emacs"
        elif [[ $edtchoice == "Gedit" ]]; then
            edtpkg="gedit"
            cmd="gedit"
        elif [[ $edtchoice == "Leafpad" ]]; then
            edtpkg="leafpad"
            cmd="leafpad"
        else
            $scriptpath/$scriptname && exit 0
        fi
    else
        echo "User choose to quit. Program terminated." && exit 0
    fi

    nvinst="$(pacman -Qq "$edtpkg")"
    if [[ $nvinst == "$edtpkg" ]]; then
        nohup $cmd $bmk > /dev/null 2>&1 &
    else
        notify-send "$edtchoice is not installed" "Install it or choose another one." \
            --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 1
    fi
fi
