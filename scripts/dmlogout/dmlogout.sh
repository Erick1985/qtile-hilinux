#!/bin/bash
##  _   _  _  _      _                      _ 
## | | | |(_)| |    (_) _ __   _   _ __  __| | 
## | |_| || || |    | || '_ \ | | | |\ \/ /| | 
## |  _  || || |___ | || | | || |_| | >  < |_| 
## |_| |_||_||_____||_||_| |_| \__,_|/_/\_\(_) 
## 
##                     https://hilinux.com.br

source $HOME/.scripts/_dmenu/dmenu-colors;

# Set with the flags "-e", "-u","-o pipefail" cause the script to fail
# if certain things happen, which is a good thing.  Otherwise, we can
# get hidden bugs that are hard to discover.
set -euo pipefail

config="${HOME}/.config/dmscripts/dmconf"
# No issues should arrise since it won't even be sourced if the file doesn't exist
# shellcheck disable=SC1090	
[ -f "${config}" ] && source "${config}"
# Define LOCKER in .xprofile to set to different LOCKER program
: "${LOCKER:="/home/smsjr/.config/i3lock/lock.sh"}"

# use notify-send if run in dumb term
# TODO: add abillity to control from config.
_out="echo"
if [[ ${TERM} == 'dumb' ]]; then
    _out="notify-send"
fi
export _out

# look up what managers are used (makes it more dynamic)
declare -a MANAGERS
while IFS= read -r manager
do
    MANAGERS+=("${manager,,}")
done < <(grep 'Name' /usr/share/xsessions/*.desktop | awk -F"=" '{print $2}')

# An array of options to choose.
declare -a options=(
"  Lock"
"  Logout"
"  Suspend"
"  Reboot"
"  Shutdown"
)

_out(){
    ${_out} "dmlogout" "$@"
}

# Piping the above array into dmenu.
# We use "printf '%s\n'" to format the array one item to a line.
choice=$(printf '%s\n' "${options[@]}" | ${DMENU} -p ' Power  ' "${@}")
    # What to do when/if we choose one of the options.
    case $choice in
	        '  Lock')
		    ${LOCKER}
    		;;
	        '  Logout')
    		if [[ "$(echo -e "No\nYes" | ${DMENU} -p "${choice}? " "${@}" )" == "Yes" ]]; then
                if [[ "$DESKTOP_SESSION" == "qtile" ]]; then
                    if pgrep -x mpd >/dev/null; then
                      mpc stop
                    fi  
                    qtile cmd-obj -o cmd -f shutdown
                elif [[ "$DESKTOP_SESSION" == "xmonad" ]]; then
                    if pgrep -x mpd >/dev/null; then
                      mpc stop
                    fi  
                    killall xmonad-x86_64-l 
                elif [[ "$DESKTOP_SESSION" == "dwm" ]]; then
                    if pgrep -x mpd >/dev/null; then
                      mpc stop
                    fi  
                    killall -q dwmblocks
                    killall dwm
                elif [[ "$DESKTOP_SESSION" == "bspwm" ]]; then
                    if pgrep -x mpd >/dev/null; then
                      mpc stop
                    fi  
                    killall bspwm sxhkd
                fi
    		else
	    	    echo "User chose not to logout." && exit 0
    		fi
	    	;;
	        '  Reboot')
    		if [[ "$(echo -e "No\nYes" | ${DMENU} -p "${choice}? " "${@}" )" == "Yes" ]]; then
                mpc stop
	    	    systemctl reboot
		        #shutdown -r now
    		else
	    	    echo "User chose not to reboot." && exit 0
		    fi
    		;;
	        '  Shutdown')
		    if [[ "$(echo -e "No\nYes" | ${DMENU} -p "${choice}? " "${@}" )" == "Yes" ]]; then
                mpc stop
    		    systemctl poweroff
	    	    #shutdown now
		    else
    		    echo "User chose not to shutdown." && exit 0
	    	fi
		    ;;
    	    '  Suspend')
	    	if [[ "$(echo -e "No\nYes" | ${DMENU} -p "${choice}? " "${@}" )" == "Yes" ]]; then
                mpc stop
    		    systemctl suspend
	    		${LOCKER}
    		else
	    	    echo "User chose not to suspend." && exit 0
		    fi
    		;;
	        'Quit')
        		echo "Program terminated." && exit 0
	        ;;
		    # It is a common practice to use the wildcard asterisk symbol (*) as a final
    	    # pattern to define the default case. This pattern will always match.
	        *)
    		exit 0
	        ;;
    esac
