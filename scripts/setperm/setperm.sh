#!/bin/bash
##  _   _  _  _      _                      _ 
## | | | |(_)| |    (_) _ __   _   _ __  __| | 
## | |_| || || |    | || '_ \ | | | |\ \/ /| | 
## |  _  || || |___ | || | | || |_| | >  < |_| 
## |_| |_||_||_____||_||_| |_| \__,_|/_/\_\(_) 
## 
##                     https://hilinux.com.br

# This script sets all files in the destination directory to be executable as program.

count=0
# My Scripts Directory
if [[ -d ~/.scripts ]]; then
    cd ~/.scripts
    # Set all the files as executable
    find . -type f -print0 | xargs -0 chmod 775
    let "count=count+1"
fi

# Qtile Directory
if [[ -d ~/.config/qtile/scripts ]]; then
    cd ~/.config/qtile/scripts
    # Set all the files as executable
    find . -type f -print0 | xargs -0 chmod 775
    let "count=count+1"
fi

# Xmonad Directory
if [[ -d ~/.xmonad/scripts ]]; then
    cd ~/.xmonad/scripts
    # Set all the files as executable
    find . -type f -print0 | xargs -0 chmod 775
    let "count=count+1"
fi

# Dwm Blocks Directory
if [[ -d ~/.sources/dwmblocks/scripts ]]; then
    cd ~/.sources/dwmblocks/scripts
    # Set all the files as executable
    find . -type f -print0 | xargs -0 chmod 775
    let "count=count+1"
fi

# Polybar Directory
if [[ -d ~/.config/polybar/scripts ]]; then
    cd ~/.config/polybar/scripts
    # Set all the files as executable
    find . -type f -print0 | xargs -0 chmod 775
    let "count=count+1"
fi
if [ $count = 5 ]; then
    notify-send "Scripts" "All permissions had set successfully." --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/process-completed-symbolic.svg" && exit 0
else
    notify-send "Scripts" "$count/5 Script(s) folders had set successfully." --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-warning-symbolic.svg" && exit 0
fi
