#!/bin/bash
##  _   _  _  _      _                      _ 
## | | | |(_)| |    (_) _ __   _   _ __  __| | 
## | |_| || || |    | || '_ \ | | | |\ \/ /| | 
## |  _  || || |___ | || | | || |_| | >  < |_| 
## |_| |_||_||_____||_||_| |_| \__,_|/_/\_\(_) 
## 
##                     https://hilinux.com.br

source $HOME/.scripts/_dmenu/dmenu-colors;

if [[ "$(echo -e "No\nYes" | ${DMENU} -p "Run FreeFileSync? ")" == "Yes" ]]; then
    status=$(ls /media/TimeCapsule | wc -l)
    if [[ $status -eq 0 ]]; then
        if ! pgrep -x afpfsd >/dev/null; then
            nohup afpfsd >/dev/null && sleep 2
        fi
        cmd=$(mount /media/TimeCapsule 2>&1)
        ### Test if mounting succeed
        if [ $? -eq 255 ]; then
          notify-send "Time Capsule" "Error mounting Time Capsule.\n\nError Message(s):\n$cmd" \
              --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 1
        else
            freefilesync && cmdu=$(umount /media/TimeCapsule 2>&1) && killall afpfsd
            if [ $? -eq 255 ]; then
                notify-send "Time Capsule" "Error unmounting Time Capsule.\n\nError Message(s):\n$cmdu" \
                    --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 1
            fi
        fi
    else
        choice="$(echo -e "No\nYes" | ${DMENU} -p "Unmount Time Capsule after sync? ")"
        if [[ $choice == "Yes" ]]; then
            freefilesync && cmdu=$(umount /media/TimeCapsule 2>&1) && killall afpfsd
            ### Test if unmounting succeed
            if [ $? -eq 255 ]; then
            notify-send "Time Capsule" "Error unmounting Time Capsule.\n\nError Message(s):\n$cmdu" \
                --icon="/usr/share/icons/Tela-circle-grey-dark/symbolic/status/dialog-error-symbolic.svg" && exit 1
            fi
        elif [[ $choice == "No" ]]; then
            nohup freefilesync 2>&1 &
        else
            echo "User chose to quit." && exit 0
        fi
    fi
else
    echo "User chose not to run FreeFileSync." && exit 0
fi
