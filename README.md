# My personal setup using Arch Linux, Qtile Window Manager, ST (Suckless Terminal), Dmenu, Rofi, i3 Lock and more.

![qtile-hilinux screenshots](https://gitlab.com/junior-sms/qtile-hilinux/-/raw/master/screenshots/title.png "screenshots")

Qtile config based on Distrotube's version (https://distro.tube | https://gitlab.com/dwt1) and ST - Suckless Terminal based on Luke Smith's edition (https://lukesmith.xyz | https://github.com/LukeSmithxyz).


## Features
- 3 Monitors Setup
- Qtile Bar (No third party bar)
- Clickable Widgets
- Scratchpads ("Invisible" Workspaces)
- Keybind Helper (like Awesome WM)
- Rounded Corners Bar and Windows
- Background Blur and Transparency
- Dracula Theme (GTK and Terminal)

## Dependencies/Recommendations
- dmenu
- rofi
- picom (Jonaburg Edition | https://github.com/jonaburg/picom)
- fehbg (Set Wallpaper)
- flameshot (Screenshots)
- gsimplecal (for Bar Calendar)
- qalculator-gtk (for Scratchpad Calculator)
- leafpad (for Scratchpad Notes)
- python-psutil (for Widgets)
- trash-cli (for Trash Widget)
- yad (for Keybinding Helper)
- i3lock-color (https://github.com/Raymo111/i3lock-color)
- qutebrowser (for Bookmarks Script)
- pass (for Passwords Script)
- Fonts
    - Ubuntu
    - mononoki Nerd Font
    - Source Code Pro
    - ttf-weather-icons 
    - FantasqueSansMono Nerd Font 

## Screenshots

### Monitor 1
![qtile-hilinux screenshots](https://gitlab.com/junior-sms/qtile-hilinux/-/raw/master/screenshots/monitor-1.png "screenshots")

### Monitor 2 (with Systray)

![qtile-hilinux screenshots](https://gitlab.com/junior-sms/qtile-hilinux/-/raw/master/screenshots/monitor-2.png "screenshots")

### Monitor 3
![qtile-hilinux screenshots](https://gitlab.com/junior-sms/qtile-hilinux/-/raw/master/screenshots/monitor-3.png "screenshots")

### Rofi Menu
![qtile-hilinux screenshots](https://gitlab.com/junior-sms/qtile-hilinux/-/raw/master/screenshots/rofi.png "screenshots")

### Rofi Menu Fullscreen
![qtile-hilinux screenshots](https://gitlab.com/junior-sms/qtile-hilinux/-/raw/master/screenshots/rofi-full.png "screenshots")

### Rofi Power Menu
![qtile-hilinux screenshots](https://gitlab.com/junior-sms/qtile-hilinux/-/raw/master/screenshots/rofi-power.png "screenshots")

### ST - Suckless Terminal
![qtile-hilinux screenshots](https://gitlab.com/junior-sms/qtile-hilinux/-/raw/master/screenshots/st-01.png "screenshots")

![qtile-hilinux screenshots](https://gitlab.com/junior-sms/qtile-hilinux/-/raw/master/screenshots/st-02.png "screenshots")

![qtile-hilinux screenshots](https://gitlab.com/junior-sms/qtile-hilinux/-/raw/master/screenshots/st-03.png "screenshots")

### Thunar File Manager
![qtile-hilinux screenshots](https://gitlab.com/junior-sms/qtile-hilinux/-/raw/master/screenshots/thunar.png "screenshots")

### Keybind Helper (Super + Control + F1)
![qtile-hilinux screenshots](https://gitlab.com/junior-sms/qtile-hilinux/-/raw/master/screenshots/keybindings-help.png "screenshots")

### Login Screen (Lightdm with Sapphire Theme)
![qtile-hilinux screenshots](https://gitlab.com/junior-sms/qtile-hilinux/-/raw/master/screenshots/login.png "screenshots")

### Lockscreen (Customized i3lock)
![qtile-hilinux screenshots](https://gitlab.com/junior-sms/qtile-hilinux/-/raw/master/screenshots/lockscreen.png "screenshots")
